// Aggregation in MongoDB and Query Case Studies.

// Inserting Data

db.fruits.insertMany([
  {
    name: "Apple",
    color: "Red",
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: ["Philippines", "US"],
  },

  {
    name: "Banana",
    color: "Yellow",
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: ["Philippines", "Ecuador"],
  },

  {
    name: "Kiwi",
    color: "Green",
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: ["US", "China"],
  },

  {
    name: "Mango",
    color: "Yellow",
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: ["Philippines", "India"],
  },
]);

// Aggregation Pipeline

// Documentation on Aggregation:
// https://www.mongodb.com/docs/manual/reference/operator/aggregation-pipeline/

// 1st Phase - $match phase is responsible for gathering the initial items based on a certain argument/criteria
// 2nd Phase - $group phase is responsible for grouping the specific fields from the documents after the criteria has been determined.
// 3rd Phase - optional - $project phase is responsible for excluding certain fields that do not need to show up in the final result.

// 1 stage only
// .count() is for counting object or specific elements or fields.

db.fruits.count();

// The $count stage returns a count of the remaining documents in the aggregation pipeline and assigns the value to a field.

db.fruits.aggregate([{ $count: "fruits" }]);

// 2nd Stage

// With $match and $count

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $count: "fruitsonSale",
  },
]);

// $match and $group

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      totalStocks: { $sum: "$stock" },
    },
  },
]);

// 3rd Phase

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      totalStocks: {
        $sum: "$stock",
      },
      price: {
        $sum: "$price",
      },
    },
  },
  {
    $project: {
      totalStocks: 0,
      _id: 0,
    },
  },
]);

// $sort operator - responsible for sorting/arranging items in the result based on their value. (1 means ascending, -1 means descending)

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      totalStocks: {
        $sum: "$stock",
      },
      price: {
        $sum: "$price",
      },
    },
  },
  {
    $project: {
      totalStocks: 0,
      _id: 0,
    },
  },
  {
    $sort: {
      price: 1,
    },
  },
]);

// $unwind operator is responsible for deconstructuring an array and using them as unique identifiers for each row in the result.

db.fruits.aggregate([
    {
        $unwind: "$origin"
    }
]);

/*
For Activity

$avg, $min, $max:

https://www.mongodb.com/docs/manual/reference/operator/aggregation/group/#mongodb-pipeline-pipe.-group

*/