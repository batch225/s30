// Fruits on sale

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $count: "fruitsonSale",
  },
]);

// Stock more than 20

db.fruits.aggregate([
  {
    $match: {
      stock: {
        $gte: 20
      }
    },
  },
  {
    $count: "enoughStock"
  },
]);

// Average

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      avg_price: {
        $avg: "$price",
        
      },
    }
  },
  {
      $sort: {
          price: 1
      }
  }
]);

// Max

db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      max_price: {
        $max: "$price",
        
      },
    }
  },
  {
      $sort: {
          price: 1
      }
  }
]);

//  Min

db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      min_price: {
        $min: "$price",
        
      },
    }
  },
  {
      $sort: {
          price: 1
      }
  }
]);

